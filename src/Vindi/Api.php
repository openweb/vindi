<?php

namespace Vindi;

class Api 
{
    private static $endpoint = 'https://app.vindi.com.br/api/v1';
    private static $user = '';
    private static $password = '';
    private static $debug = false;

    public static function setDebug($debug) {
        static::$debug = $debug;
    }

    public static function getDebug() {
        return static::$debug;
    }

    public static function setEndpoint($endpoint) {
        static::$endpoint = $endpoint;
    }

    public static function getEndpoint() {
        return static::$endpoint;
    }

    public static function setUser($user) {
        static::$user = $user;
    }

    public static function getUser() {
        return static::$user;
    }

    public static function setPassword($password) {
        static::$password = $password;
    }

    public static function getPassword() {
        return static::$password;
    }

    public static function setup($api_key) {
        static::setUser($api_key);
    }


    public static function get($path, $query = array()) {
        $endpoint = static::getEndpoint();
        $user = static::getUser();
        $password = static::getPassword();

        $url = $endpoint.'/'.ltrim($path,'/');

        if (!empty($query)) {
            $url .= '?'.http_build_query($query);
        }

        if (static::getDebug()) {
            var_dump($url);
        }
        

        $ch = curl_init($url);

        curl_setopt_array(
            $ch,
            array(
                CURLOPT_USERPWD => $user.':'.$password,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                ),
                CURLOPT_RETURNTRANSFER => TRUE
            )
        );

        $result = curl_exec($ch);
        if (static::getDebug()) {
            var_dump($result);
        }
        

        if (curl_errno($ch)) {
            curl_close($ch);
            throw new \Exception(curl_error($ch));
        } else {
            $info = curl_getinfo($ch);
            $http_code = (int) $info['http_code'];
            if (empty($http_code)) {
                throw new \Exception("No HTTP code was returned.");
            }
            switch ($http_code) {
                case 200:
                case 201:
                    break;
                case 401:
                    throw new \Exception("Unauthorized", 401);
                    break;
                case 402:
                    throw new \Exception("Payment Required", 402);
                    break;
                case 403:
                    throw new \Exception("Forbidden", 403);
                    break;
                case 404:
                    throw new \Exception("Not Found", 404);
                    break;
                case 406:
                    throw new \Exception("Not Acceptable", 406);
                    break;
                case 422:
                    throw new \Exception("Unprocessable Entity", 422);
                    break;
                case 429:
                    throw new \Exception("Too Many Requests", 429);
                    break;
                case 400:
                    throw new \Exception("Bad Request", 400);
                    break;
                case 500:
                    throw new \Exception("Internal Server Error", 500);
                    break;
                default:
                    throw new \Exception("Unexpected Error", $http_code);
                    break;
            }
        }
        curl_close($ch);
        return json_decode($result);
    }

    public static function request($method, $path, $body = array(), $query = array())
    {
        $endpoint = static::getEndpoint();
        $user = static::getUser();
        $password = static::getPassword();

        $url = $endpoint . '/' . ltrim($path, '/');

        if (!empty($query)) {
            $url .= '?' . http_build_query($query);
        }

        if (static::getDebug()) {
            var_dump($url);
        }

        $ch = curl_init($url);

        $body = json_encode($body);
        $length = strlen($body);

        if (static::getDebug()) {
            var_dump($body);
        }

        curl_setopt_array(
            $ch,
            array(
                CURLOPT_USERPWD => $user . ':' . $password,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    "Content-Length: {$length}"
                ),
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_POSTFIELDS => $body
            )
        );
        $result = curl_exec($ch);
        
        if (static::getDebug()) {
            var_dump($result);
        }

        if (curl_errno($ch)) {
            curl_close($ch);
            throw new \Exception(curl_error($ch));
        } else {
            $info = curl_getinfo($ch);
            $http_code = (int)$info['http_code'];
            if (empty($http_code)) {
                throw new \Exception("No HTTP code was returned.");
            }
            switch ($http_code) {
                case 200:
                case 201:                    
                    break;
                case 401:
                    throw new \Exception("Unauthorized", 401);
                    break;
                case 402:
                    throw new \Exception("Payment Required", 402);
                    break;
                case 403:
                    throw new \Exception("Forbidden", 403);
                    break;
                case 404:
                    throw new \Exception("Not Found", 404);
                    break;
                case 406:
                    throw new \Exception("Not Acceptable", 406);
                    break;
                case 422:
                    throw new \Exception("Unprocessable Entity", 422);
                    break;
                case 429:
                    throw new \Exception("Too Many Requests", 429);
                    break;
                case 400:
                    throw new \Exception("Bad Request", 400);
                    break;
                case 500:
                    throw new \Exception("Internal Server Error", 500);
                    break;
                default:
                    throw new \Exception("Unexpected Error", $http_code);
                    break;
            }
        }
        curl_close($ch);
        return json_decode($result);
    }

    public static function post($path, $body = array(), $query = array()) {
        return static::request('POST', $path, $body, $query);
    }
    
    public static function put($path, $body = array(), $query = array()) {
        return static::request('PUT', $path, $body, $query);
    }

    public static function patch($path, $body = array(), $query = array()) {
        return static::request('PATCH', $path, $body, $query);
    }
    public static function delete($path, $body = array(), $query = array()) {
        return static::request('DELETE', $path, $body, $query);
    }
}
