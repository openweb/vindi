<?php

namespace Vindi;

class Bills {

    /**
     * Returna uma lista de faturas
     * 
     * @param int $page Page of results to fetch.
     * @param int $per_page Number of results to return per page.
     * @param array $query Filtro para busca:
     *      id, code, installments, period_id, subscription_id, customer_id, amount, status, payment_method_id, seen_at, due_at, billing_at, created_at, updated_at
     * @param string $sort_by Atributo opcional para ordenação
     * @param string $sort_order Sentido opcional para ordenação
     * 
     * @return array  
     */
    public static function get($page = 1, $per_page = 25, $query = array(), $sort_by = 'created_at', $sort_order = 'asc') {
        return Api::get('/bills', array(
            'page' => $page,
            'per_page' => $per_page,
            'query' => join(' ', $query),
            'sort_by' => $sort_by,
            'sort_order' => $sort_order
        ));
    }

    /**
     * Retorna uma fatura específica através do ID
     * 
     * @param int $id ID da fatura que deverá ser retornada
     * 
     * @return object
     */
    public static function load($id) {
        return Api::get("/bills/{$id}");
    }

    /**
     * Cancela uma fatura através do ID
     * 
     * @param int $id ID da fatura a ser cancelada
     * 
     * @return object
     */
    public static function delete($id) {
        return Api::delete("/bills/{$id}");
    }


    /**
     * Cria uma nova fatura avulsa ou atualiza uma fatura existente.
     * 
     * @param object $bill
     * 
     * @return object
     */
    public static function save($bill) {
        if (isset($bill->id) && $bill->id != 0) {
            return static::update($bill);
        }
        return static::create($bill);
    }

    /**
     * Atualiza uma fatura existente
     * 
     * @param object $bill
     * 
     * @return object
     */
    public static function update($bill) {
        return Api::put("/bills/{$bill->id}", $bill);
    }

    /**
     * Cria uma nova fatura avulsa
     * 
     * @param object $bill
     * 
     * @return object
     */
    public static function create($bill) {
        return Api::post("/bills", $bill);
    }

    /**
     * Aprova uma fatura através do ID
     * 
     * @param int $id ID da fatura que será aprovada
     * 
     * @return object
     */
    public static function approve($id) {
        return Api::post("/bills/{$id}/approve");
    }

    /**
     * Retorna os itens de fatura através de seu ID
     * 
     * @param int $id ID da fatura cujos itens deverão ser retornados.
     * @param int $page Page of results to fetch.
     * @param int $per_page Number of results to return per page.
     * 
     * @return array
     */
    public static function getItems($id, $page = 1, $per_page = 25) {
        return Api::get("/bills/{$id}/bill_items", array(
            'page' => $page,
            'per_page' => $per_page
        ));
    }

    /**
     * Antecipa imediatamente a cobrança de uma fatura através do ID
     * 
     * @param int $id ID da fatura que será antecipada.
     * 
     * @return object
     */
    public static function charge($id) {
        return Api::post("/bills/{$id}/charge");
    }

    /**
     * Gera notas fiscais de uma fatura através do ID
     * 
     * @param int $id ID da fatura que irá gerar as notas fiscais.
     * 
     * @return object
     */
    public static function invoice($id) {
        return Api::post("/bills/{$id}/invoice");
    }

}