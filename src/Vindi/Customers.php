<?php

namespace Vindi;

class Customers {
    
    /**
     * Retorna uma lista de clientes
     * 
     * @param int $page Page of results to fetch.
     * @param int $per_page Number of results to return per page.
     * @param array $query Filtro para busca:
     *      id, name, status, code, email, registry_code, created_at, updated_at
     * @param string $sort_by Atributo opcional para ordenação
     * @param string $sort_order Sentido opcional para ordenação
     * 
     * @return array      
     */
    public static function get($page = 1, $per_page = 25, $query = array(), $sort_by = 'created_at', $sort_order = 'asc') {
        return Api::get('/customers', array(
            'page' => $page,
            'per_page' => $per_page,
            'query' => join(' ', $query),
            'sort_by' => $sort_by,
            'sort_order' => $sort_order
        ));
    }

    /**
     * Retorna um cliente específico através do ID
     * 
     * @param int $id ID do cliente que deverá ser retornado.
     * 
     * @return object 
     */
    public static function load($id) {
        return Api::get("/customers/{$id}");
    }

    /**
     * Cadastra um novo cliente ou atualiza um existente
     * 
     * @param object $customer
     * 
     * @return object
     */
    public static function save($customer) {
        if (isset($customer->id) && $customer->id != 0) {
            return static::update($customer);
        }
        return static::create($customer);
    }

    /**
     * Cadastra um novo cliente
     * 
     * @param object $customer
     * 
     * @return object
     */
    public static function create($customer) {
        return Api::post('/customers', $customer);
    }

    /**
     * Atualiza um cliente existente
     * 
     * @param object $customer
     * 
     * @return object
     */
    public static function update($customer) {
        return Api::put("/customers/{$customer->id}", $customer);
    }
    
    /**
     * Arquiva um cliente específico através do ID
     * 
     * @param int $id
     * 
     * @return object
     */
    public static function delete($id) {
        return Api::delete("/customers/{$id}");
    }

    /**
     * Reverte o arquivamento de um cliente.
     * 
     * @param int $id
     * 
     * @return object
     */
    public static function unarchive($id) {
        return Api::post("/customers/{$id}/unarchive");
    }
}