<?php

namespace Vindi;

class Products
{

    /**
     * Retorna uma lista de produtos
     * 
     * @param int $page Page of results to fetch.
     * @param int $per_page Number of results to return per page.
     * @param array $query Filtro para busca:
     *      id,name, code, status, invoice, unit, pricing_schema_id, created_at, updated_at, schema_type, price
     * @param string $sort_by Atributo opcional para ordenação
     * @param string $sort_order Sentido opcional para ordenação
     * 
     * @return array      
     */
    public static function get($page = 1, $per_page = 25, $query = array(), $sort_by = 'created_at', $sort_order = 'asc')
    {
        return Api::get('/products', array(
            'page' => $page,
            'per_page' => $per_page,
            'query' => join(' ', $query),
            'sort_by' => $sort_by,
            'sort_order' => $sort_order
        ));
    }

    /**
     * Retorna um produto específico através do ID
     * 
     * @param int $id ID do produto que deverá ser retornado.
     * 
     * @return object 
     */
    public static function load($id)
    {
        return Api::get("/products/{$id}");
    }

    /**
     * Cadastra um novo produto ou atualiza um existente
     * 
     * @param object $product
     * 
     * @return object
     */
    public static function save($product)
    {
        if (isset($product->id) && $product->id != 0) {
            return static::update($product);
        }
        return static::create($product);
    }

    /**
     * Cadastra um novo produto
     * 
     * @param object $product
     * 
     * @return object
     */
    public static function create($product)
    {
        return Api::post('/products', $product);
    }

    /**
     * Atualiza um produto existente
     * 
     * @param object $product
     * 
     * @return object
     */
    public static function update($product)
    {
        return Api::put("/products/{$product->id}", $product);
    }

}
