<?php

namespace Vindi;

class PaymentMethods {
    /**
     * Lista todos os métodos de pagamento disponíveis.
     * 
     * @param int $page Page of results to fetch.
     * @param int $per_page Number of results to return per page.
     * @param array $query Filtro para busca:
     *      id, name, code
     * @param string $sort_by Atributo opcional para ordenação
     * @param string $sort_order Sentido opcional para ordenação
     * 
     * @return array  
     */
    public static function get($page = 1, $per_page = 25, $query = array(), $sort_by = 'id', $sort_order = 'asc') {
        return Api::get('/payment_methods', array(
            'page' => $page,
            'per_page' => $per_page,
            'query' => join(' ',$query),
            'sort_by' => $sort_by,
            'sort_order' => $sort_order
        ));
    }

    /**
     * Returna um método de pagamento específico através do ID
     * 
     * @param int $id
     * 
     * @return object
     */
    public static function load($id) {
        return Api::get("/payment_methods/{$id}");
    }
}